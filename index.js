// [Section] - if , else if, else statements

let numG = -1;

// if Statement
// Executes a statement if a specified condition is true

// This will not execute since the condition is not met or it is "false"
if (numG < -5) {
    console.log("Hello");
}

// This will execute since the condition was met or it is "true"
if (numG < 1) {
    console.log("Hello");
}

// else if statement
/* 
1. it executes a statement if previous condition are false and if the specified condition is true
2. The "else if" clause is optional and can be added to capture additional conditions


*/

let numH = 1;

if (numG > 0) {
    console.log("Hello");
} else if (numH > 0) {
    console.log("world");
}

// else statement

/* 
1. Executes a statement if all other conditions are false
2. The "else" is optional and can be added to capture any other results to change the flow of the program

 */

if (numG > 0) {
    console.log("Hello");
} else if (numH == 0) {
    console.log("world");
} else {
    console.log("Again");
}

// if, else if, and else statements with functions

let message = "No Message";

console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return "Not a typhoon yet.";
    } else if (windSpeed <= 61) {
        return "Tropical Depression detected.";
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical Storm detected.";
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return "Severe Tropical Storm detected.";
    } else {
        return "Typhoon detected.";
    }
}

// Return the string to the variable "message" that invoked it

message = determineTyphoonIntensity(70);
console.log(message);
if (message == "Tropical Storm detected.") {
    console.warn(message);
}

message = determineTyphoonIntensity(112);
console.log(message);

message = determineTyphoonIntensity(120);
console.log(message);

// [Section] - Truthy and Falsy
/* 
    - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise
    - Falsy values/exceptions for truthy:
        1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN
*/

// Truthy Examples
if (true) {
    console.log("Truthy");
}

if (1) {
    console.log("Truthy");
}

if ([]) {
    console.log("Truthy");
}

// Falsy Examples

if (false) {
    console.log("Truthy");
}
if (0) {
    console.log("Truthy");
}

if (undefined) {
    console.log("Truthy");
}

if (undefined) {
    console.log("Truthy");
}

// [Section] - Conditional (Ternary) Operator
/* 
    - The Conditional (Ternary) Operator takes in three
    operands :
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else"
    statement
    - Ternary operators have an implicit "return"
    statement meaning that without the "return" keyword,
    the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where
    the result consists of only one line of code
    For multiple lines of code/ code blocks, a function
    may be defined then used in a ternary operator
    - Syntax
    (expression) ? ifTrue : ifFa1se; 
*/

// Single Statement Execution
let ternaryResult1 = 1 < 18 ? true : false;
console.log(`Result of ternary operator is: ${ternaryResult1}`);
let ternaryResult2 = 100 < 18 ? true : false;
console.log(`Result of ternary operator is: ${ternaryResult2}`);

// Multiple Statements Execution
// Both functions perform two seperate tasks which changes the value of the "name" variable and returns the result storing it i the "legalAge" variable

let name;

function isOfLegalAge() {
    name = "John";
    return "Your are of the legal age limit";
}

function isUnderAge() {
    name = "Jane";
    return "You are under the age limit";
}
let age = parseInt(prompt("What is your Age?"));

console.log(typeof age);
console.log(age);

let legalAge = age > 18 ? isOfLegalAge() : isUnderAge();
console.log(`The result of ternary operator: ${legalAge} ${name}`);

// [Section] - Switch Statement
/* 
    - Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
    - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
    - The "expression" is the information used to match the "value" provided in the switch cases
    - Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
    - Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
    - The "break" statement is used to terminate the current loop once a match has been found
    - Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
    - Syntax
        switch (expression) {
            case value:
                statement;
                break;
            default:
                statement;
                break;
        }
*/

let day = prompt("What day of the week is today?").toLowerCase();

console.log(day);

switch (day) {
    case `monday`:
        console.log("The color of the day is Red");
        break;
    case `tuesday`:
        console.log("The color of the day is Orange");
        break;
    case `wednesday`:
        console.log("The color of the day is Yellow");
        break;
    case `thursday`:
        console.log("The color of the day is Green");
        break;
    case `friday`:
        console.log("The color of the day is Blue");
        break;
    case `saturday`:
        console.log("The color of the day is Indigo");
        break;
    case `sunday`:
        console.log("The color of the day is Violet");
        break;
    default:
        alert("Please input a valid day");
        break;
}

// [Section] - Try-Catch-Finally Statements

function showIntensityAlert(windSpeed) {
    try {
        //  Try to attempt to execute a particular code
        alert(determineTyphoonIntensity(windSpeed));
    } catch (error) {
        console.log(typeof error);
        console.warn(error.message);
    } finally {
        alert("Intensity updates will show you alert");
    }
}

showIntensityAlert(56);
